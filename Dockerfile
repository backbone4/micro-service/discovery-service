FROM openjdk:17-jdk-alpine
EXPOSE 8761
ENV HOSTNAME=localhost
ENV DISCOVERY_URL=http://localhost:8671/eureka
RUN apk add --no-cache maven
WORKDIR /service-discovery
COPY ./ ./
RUN mvn package
CMD ["java", "-jar","/service-discovery/target/service-discovery-0.0.1-SNAPSHOT.jar"]
